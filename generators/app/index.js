var Generator = require('yeoman-generator');
var prompts = require('./templates/prompts');
var yosay = require('yosay');
var chalk = require('chalk');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
  }

  initializing() {
    this.yarnInstall(['rollup', 'rollup-plugin-node-resolve',
                      'rollup-plugin-commonjs', 'rollup-plugin-amd',
                      'rollup-watch'], {dev: true});
  }

  prompting() {
    this.log(yosay(
      `Welcome to the ${chalk.green('rollup-config')}!`
    ));

    return this.prompt(prompts).then(answers => {
      this.answers = answers;
    });
  }

  addDependencies() {
    if (this.answers.compiler === 'babel') {
      this.yarnInstall(['rollup-plugin-babel', 'babel-preset-env',
                        'babel-plugin-external-helpers'], {dev: true});
    } else if (this.answers.compiler === 'buble') {
      this.yarnInstall(['rollup-plugin-buble'], {dev: true});
    }

    if (this.answers.alternativeJS.includes('coffee')) {
      this.yarnInstall(['rollup-plugin-coffee-script'], {dev: true});
    }

    if (this.answers.alternativeJS.includes('pure')) {
      this.yarnInstall(['rollup-plugin-purs'], {dev: true});
    }

    switch (this.answers.framework) {
      case 'vue':
        this.yarnInstall(['rollup-plugin-vue'], {dev: true});
        break;
      case 'angular':
        this.yarnInstall(['rollup-plugin-angular', 'rollup-plugin-typescript2'],
                         {dev: true});
        break;
      case 'ractive':
        this.yarnInstall(['ractive']);
        this.yarnInstall(['rollup-plugin-ractive'], {dev: true});
        break;
      case 'riot':
        this.yarnInstall(['rollup-plugin-riot'], {dev: true});
        break;
      case 'svelte':
        this.yarnInstall(['rollup-plugin-svelte'], {dev: true});
        break;
      case 'jsx':
        this.yarnInstall(['rollup-plugin-jsx'], {dev: true});
        break;
    }
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('rollup.config.js'),
      this.destinationPath('rollup.config.js'),
      this.answers
    );

    if (this.answers.compiler === 'babel') {
      this.fs.copy(
        this.templatePath('.babelrc'),
        this.destinationPath('.babelrc')
      );
    }

    this.fs.extendJSON(
      this.destinationPath('package.json'),
      {
        scripts: {
          build: "rollup -c",
          dev: "rollup -c -w"
        }
      }
    );
    this.log(chalk.green(`Adding useful scripts to a ${chalk.white('package.json')} file.`));
    this.log(chalk.green(`Run ${chalk.bold('bundle')} to watch your files and compile them when they change. Run ${chalk.bold('lint')} to lint all of your files.`));
  }

  install() {
    this.installDependencies({
      yarn: true,
      npm: true,
      bower: false
    });
  }

  end() {
    this.log(chalk.green('... We are done!'));
    this.log(chalk.green(`Thanks for using ${chalk.bold('rollup-config')}. Hava a nice day!`));
  }
}
