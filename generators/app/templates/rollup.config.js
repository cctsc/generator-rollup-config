import commonjs from 'rollup-plugin-commonjs';
import amd from 'rollup-plugin-amd';
import resolve from 'rollup-plugin-node-resolve';

<% if (compiler === 'babel') { %>
  import babel from 'rollup-plugin-babel';
<% } else if (compiler === 'buble') { %>
  import buble from 'rollup-plugin-buble';
<% } %>

<% if (framework === 'vue') { %>
  import vue from 'rollup-plugin-vue';
<% } else if (framework === 'angular') { %>
  import angular from 'rollup-plugin-angular';
  import typescript from 'rollup-plugin-typescript2';
<% } else if (framework === 'ractive') { %>
  import ractive from 'rollup-plugin-ractive';
<% } else if (framework === 'riot') { %>
  import riot  from 'rollup-plugin-riot'
<% } else if (framework === 'svelte') { %>
  import svelte from 'rollup-plugin-svelte';
<% } else if (framework === 'jsx') { %>
  import jsx from 'rollup-plugin-jsx';
<% } %>

<% if (alternativeJS.includes('coffee')) { %>
  import coffeescript from 'rollup-plugin-coffee-script';
<% } %>
<% if (alternativeJS.includes('pure')) { %>
  import purs from 'rollup-plugin-purs';
<% } %>
<% if (alternativeJS.includes('type')) { %>
  import typescript from 'rollup-plugin-typescript2';
<% } %>

export default {
  entry: '<%= entry %>',
  dest: '<%= dest %>',
  format: '<%= format %>',
  plugins: [
    <% if (alternativeJS.includes('type')) { %>
      typescript(),
    <% } %>
    <% if (alternativeJS.includes('coffee')) { %>
      coffeescript(),
    <% } %>
    resolve({
      jsnext: true,
      main: true,
      preferBuiltins: false
      <% if (alternativeJS.includes('coffee')) { %>
      , extensions: ['.js', '.coffee']
      <% } %>
    }),
    commonjs(
      <% if (alternativeJS.includes('coffee')) { %>
      extensions: ['.js', '.coffee']
      <% } %>
    ),
    amd()
    <% if (alternativeJS.includes('pure')) { %>
      , purs()
    <% } %>
    <% if (framework === 'vue') { %>
      , vue()
    <% } else if (framework === 'angular' ) { %>
      , angular(),
      typescript()
    <% } else if (framework === 'ractive' ) { %>
      , ractive()
    <% } else if (framework === 'riot' ) { %>
      , riot()
    <% } else if (framework === 'svelte' ) { %>
      , svelte()
    <% } else if (framework === 'jsx' ) { %>
      , jsx()
    <% } %>
    <% if (compiler === 'babel') { %>,
      , babel({
        exclude: 'node_modules/**'
      })
    <% } else if (compiler === 'buble') { %>
      , buble()
    <% } %>
  ]
  <% if (false) { %>,
  externals: ['']
  <% } %>
  <% if (sourcemap) { %>,
  sourceMap: true
  <% } %>
};
