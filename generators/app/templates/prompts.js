'use strict';

module.exports = [
  {
    message: 'Where is the project entry file?',
    type: 'input',
    name: 'entry',
    default: 'src/index.js'
  },
  {
    message: 'Where to generate a bundle file?',
    type: 'input',
    name: 'dest',
    default: 'dest/bundle.js'
  },
  {
    message: 'Choose format for your bundle:',
    type: 'list',
    name: 'format',
    default: 'umd',
    choices: [
      {name: 'umd', value: 'umd'},
      {name: 'iife', value: 'iife'},
      {name: 'es', value: 'es'},
      {name: 'cjs', value: 'cjs'},
      {name: 'amd', value: 'amd'}
    ]
  },
  {
    message: 'Babel or Bubel?',
    type: 'list',
    name: 'compiler',
    default: 'babel',
    choices: [
      {name: 'Babel', value: 'babel'},
      {name: 'Buble', value: 'buble'},
      {name: 'Neither', value: false}
    ],
    store: true
  },
  {
    message: 'Do you require any of these frameworks/extensions plugins?',
    type: 'list',
    name: 'framework',
    default: false,
    choices: [
      {
        name: 'Vue - transform Vue components into a plain JS module',
        value: 'vue'
      },
      {
        name: 'Angular - angular2 template and styles inliner',
        value: 'angular'
      },
      {
        name: 'Ractive - precompile Ractive components',
        value: 'ractive'
      },
      {
        name: 'Riot - compile Riot.js tag file',
        value: 'riot'
      },
      {
        name: 'Svelte - compile Svelte components',
        value: 'svelte'
      },
      {
        name: 'JSX - a simple wrapper around jsx-transform',
        value: 'jsx'
      },
      {name: 'No, thanks', value: false}
    ]
  },
  {
    message: 'Do you use a JS language variant?',
    type: 'checkbox',
    name: 'alternativeJS',
    choices: [
      {name: 'TypeScript', value: 'type'},
      {name: 'CoffeeScript', value: 'coffee'},
      {name: 'PureScript', value: 'pure'}
    ],
    default: []
  },
  {
    message: 'Sourcemap?',
    type: 'list',
    name: 'sourcemap',
    default: true,
    choices: [
      {name: 'Yes, please', value: true},
      {name: 'No, thank you', value: false}
    ]
  }
];
