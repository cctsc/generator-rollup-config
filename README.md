# generator-rollup-config [Deprecated]

> generator-rollup-config has been deprecated.

> A [Yeoman](http://yeoman.io/) generator for installing and configuring [Rollup](https://rollupjs.org/).

JS bundling can be a really complicated thing, but Rollup can make it easier.  
This generator will help you to quickly get started with Rollup.

## Installation

Make sure you have [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed.
Then install [Yeoman](http://yeoman.io/) and generator:
```bash
npm install -g yo
npm install -g generator-rollup-config
```

Create a new directory for your project:
```bash
mkdir my-new-project
cd my-new-project
``` 

Now you can install and configure Rollup with:
```bash
yo rollup-config
```

## Usage

Rollup-config will always install and configure: rollup, rollup-plugin-node-resolve, 
rollup-plugin-commonjs, rollup-plugin-amd, rollup-watch.  

Additionally it may provide code transpilation (buble or babel), plugins for 
working with some JS frameworks and basic support for different languages 
(Typescript, PureScript, CoffeeScript).  

It's pretty straightforward, but remember that this generator creates a very 
simple configuration. You may want to modify the configuration files 
(.babelrc, rollup.config.js) and tweak them to your needs.

### Scripts

Bundle:
```bash
npm run build
```

Watch files for change:
```bash
npm run dev
```

## Tests
You can run simple tests with:
```bash
npm test
```

Additionally you can check code with ESLint:
```bash
npm run eslint
```

## License
MIT © [Paweł Halczuk](https://bitbucket.org/cctsc/)
