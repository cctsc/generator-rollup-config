var helpers = require('yeoman-test');
var assert = require('yeoman-assert');
var path = require('path');

var promptAnswers = {};

describe('rollup-config', function() {
  before(function() {
    this.timeout(0);

    return helpers.run(path.join(__dirname, '../generators/app'))
                  .withPrompts(promptAnswers);
  });

  it('the generator can be required without throwing', function() {
    require('../generators/app/index.js');
  });

  it('generate config files', function() {
    assert.file(['.babelrc', 'rollup.config.js']);
  });

  it('add scripts to package.json', function() {
    assert.jsonFileContent(
      'package.json',
      {
        scripts: {
          build: "rollup -c",
          dev: "rollup -c -w"
        }
      }
    );
  });
});
